<?php

namespace Intrazero\GeneralNotification\Providers;

use Illuminate\Support\ServiceProvider;

class GeneralNotificationProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}