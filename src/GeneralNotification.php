<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Kutia\Larafirebase\Messages\FirebaseMessage;
use App\Jobs\TestEmailJob;
use Modules\User\Models\User;
//implements ShouldQueue
class GeneralNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $data;
    public $url;
   
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    //
    public function __construct($data)
    {
        // $this->afterCommit();
      
        $this->data= $data;
        if (isset($this->data['url'])) {
            $this->url=$this->data['url'];
            }
        else{
            $this->url="http://127.0.0.1:8000/";
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    { 
        if (isset($this->data['type'])) {
        return $this->data['type'];
        }
        else{
            return ['mail','database','firebase'];
        }
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       
        $mail = (new MailMessage);
        if (isset($this->data['template'])) {

            $mail->subject('GLO Notification')->view("mails." . $this->data['template'], ['message2' => $this->data['message'], 'user_name' => $notifiable->name]);
        } else {
            $mail->greeting("Hello, ".$notifiable->name)->line($this->data['message'])->subject('GLO Notification')->action('Notification Action', $this->url)->line('Thank you for using our application!');
        }


        return $mail;

       
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {  
        return [
            "user"=>$notifiable->name,
            "title"=>$this->data['title'],
            "message"=>$this->data['message'],
        ];
    }
    public function toFirebase($notifiable)
    {   
        return (new FirebaseMessage)
            ->withTitle($this->data['title'])
            ->withBody($this->data['message'])
            ->withPriority('high')->asMessage($notifiable->fcm);
    }
}
