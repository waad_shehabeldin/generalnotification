importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');
   
firebase.initializeApp({
    apiKey: "AIzaSyCmVeZOt9RK175nLc3So5HDAPL63KQ4O6k",
    projectId: "test-34ac2",
    messagingSenderId: "632545081611",
    appId: "1:632545081611:web:50610a800a6209036cca0e"
});
  
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function({data:{title,body,icon}}) {
    return self.registration.showNotification(title,{body,icon});
});